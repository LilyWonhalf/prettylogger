module.exports = {
    INFO: 'info',
    WARNING: 'warning',
    ERROR: 'error',
    NOTICE: 'notice'
};