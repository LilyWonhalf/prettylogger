module.exports = {
    RESET: '\x1b[0m',
    BOLD_ON: '\x1b[1m',
    BOLD_OFF: '\x1b[22m',
    INVERSE_ON: '\x1b[7m',
    FOREGROUND_BLACK: '\x1b[30m',
    FOREGROUND_RED: '\x1b[31m',
    FOREGROUND_GREEN: '\x1b[32m',
    FOREGROUND_YELLOW: '\x1b[33m',
    FOREGROUND_BLUE: '\x1b[34m',
    FOREGROUND_MAGENTA: '\x1b[35m',
    FOREGROUND_CYAN: '\x1b[36m',
    FOREGROUND_WHITE: '\x1b[33m',
    BACKGROUND_RED: '\x1b[41m',
    BACKGROUND_GREEN: '\x1b[42m'
};